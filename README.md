Dear reviewer! There are some notes about some details of implementation of test assignment:

1. Application supports following configuration changes: screen rotation, dark mode.
2. Application checks for availability of Google Play Services for retrieving device location and shows appropriate logic if they are outdated or missing.
3. Application handles disabled location services on startup.
4. Application draws it's content behind system bars and handles window insets.
5. I used MVI architecture with some adjustments to achieve loose coupling. Main component that connects logic alltogether is Store. Another vital components are: Actor(performs logic required to fetch data for the screen) and Reducer(maps data to screen state). Such components as EventProducer and ActionPreProcessor are optional depending on your way to communicate with UI layer and apply some preparations for incoming actions.
