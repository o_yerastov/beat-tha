package com.example.beattha

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.beattha.mvi.Event
import com.example.beattha.mvi.Store
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

abstract class BaseViewModel<State : Any, Action : Any>(  private val store: Store<State, *, Action>) : ViewModel() {

    private val _state = MutableStateFlow(store.initialState)
    val state: Flow<State> get() = _state

    private val _events = MutableSharedFlow<Event>(extraBufferCapacity = 3)
    val events: Flow<Event> get() = _events

    protected val stateValue: State get() = _state.value

    init {
        store.collectState()
            .onEach(::updateState)
            .launchIn(viewModelScope)

        store.collectEvents()
            .onEach(::emitEvent)
            .launchIn(viewModelScope)
    }

    private fun emitEvent(event: Event) {
        _events.tryEmit(event)
    }

    private fun updateState(state: State) {
        _state.value = state
    }

    fun acceptAction(action: Action) {
        store.acceptAction(action)
    }
}
