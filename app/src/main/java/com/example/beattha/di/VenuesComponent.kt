package com.example.beattha.di

import com.example.beattha.presentation.venues.VenuesFragment
import com.example.domain.location.LocationRepository
import com.example.domain.venue.VenueRepository
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component
interface VenuesComponent {

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance venueRepository: VenueRepository,
            @BindsInstance locationRepository: LocationRepository,
        ): VenuesComponent
    }

    fun inject(target: VenuesFragment)
}
