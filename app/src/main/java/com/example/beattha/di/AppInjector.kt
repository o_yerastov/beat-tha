package com.example.beattha.di

import android.content.Context
import com.example.data.di.DaggerDataComponent
import com.example.data.di.DataComponent

object AppInjector {

    private lateinit var appComponent: AppComponent
    private lateinit var dataComponent: DataComponent
    private lateinit var venuesComponent: VenuesComponent

    fun init(context: Context) {
        appComponent = DaggerAppComponent.factory().create(context)
        dataComponent = DaggerDataComponent.factory().create(context)
        venuesComponent =
            DaggerVenuesComponent.factory().create(dataComponent.venueRepository, dataComponent.locationRepository)
    }

    fun getVenuesComponent(): VenuesComponent {
        return venuesComponent
    }
}
