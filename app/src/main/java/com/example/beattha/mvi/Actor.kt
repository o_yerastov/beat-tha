package com.example.beattha.mvi

import kotlinx.coroutines.flow.Flow

fun interface Actor<Action : Any, Effect : Any> : (Action) -> Flow<Effect> {
    override fun invoke(action: Action): Flow<Effect>
}
