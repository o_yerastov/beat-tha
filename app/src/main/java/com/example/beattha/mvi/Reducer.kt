package com.example.beattha.mvi

fun interface Reducer<Effect : Any, State : Any> : (State, Effect) -> State {
    override fun invoke(currentState: State, effect: Effect): State
}
