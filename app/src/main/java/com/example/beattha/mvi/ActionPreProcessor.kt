package com.example.beattha.mvi

import kotlinx.coroutines.flow.Flow

fun interface ActionPreProcessor<Action : Any> : (Flow<Action>) -> Flow<Action> {
    override fun invoke(actionFlow: Flow<Action>): Flow<Action>
}
