package com.example.beattha.mvi

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.DEFAULT_CONCURRENCY
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.sync.Mutex

abstract class Store<State : Any, Effect : Any, Action : Any>(
    val initialState: State,
    private val actor: Actor<Action, Effect>,
    private val reducer: Reducer<Effect, State>,
    private val eventProducer: EventProducer<Effect>? = null,
    private val actionPreProcessor: ActionPreProcessor<Action>? = null,
) {

    private val stateFlow = MutableStateFlow(initialState)
    private val actionFlow = MutableSharedFlow<Action>(extraBufferCapacity = 5)
    private val eventFlow = MutableSharedFlow<Event>(extraBufferCapacity = 5)

    fun collectState(): Flow<State> {
        val actionsFlow = actionPreProcessor?.invoke(actionFlow) ?: actionFlow
        return actionsFlow
            .flatMapMerge(DEFAULT_CONCURRENCY, actor::invoke)
            .flowOn(Dispatchers.IO)
            .onEach { effect ->
                val event = eventProducer?.invoke(effect)
                event?.let(eventFlow::tryEmit)
            }
            .map { effect ->
                val newState = reducer(stateFlow.value, effect)
                stateFlow.emit(newState)
                newState
            }
            .distinctUntilChanged()
    }

    fun acceptAction(action: Action) {
        actionFlow.tryEmit(action)
    }

    fun collectEvents(): Flow<Event> {
        return eventFlow
    }
}
