package com.example.beattha.mvi

fun interface EventProducer<Effect : Any> : (Effect) -> Event? {
    override fun invoke(effect: Effect): Event?
}
