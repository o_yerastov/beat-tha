package com.example.beattha.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

fun <T> LifecycleOwner.observe(flow: Flow<T>, block: (T) -> Unit) {
    flow.onEach { block(it) }
        .launchIn(this.lifecycleScope)
}
