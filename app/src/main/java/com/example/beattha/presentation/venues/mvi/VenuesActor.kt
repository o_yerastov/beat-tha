package com.example.beattha.presentation.venues.mvi

import com.example.beattha.mvi.Actor
import com.example.beattha.presentation.venues.ScreenMode
import com.example.domain.NetworkNotAvailableError
import com.example.domain.location.GoogleServicesUnavailableError
import com.example.domain.location.LatLon
import com.example.domain.location.LocationDisabledError
import com.example.domain.location.LocationRepository
import com.example.domain.venue.Venue
import com.example.domain.venue.VenueRepository
import com.google.android.gms.common.api.ResolvableApiException
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

internal class VenuesActor @Inject constructor(
    private val venueRepository: VenueRepository,
    private val locationRepository: LocationRepository,
) : Actor<VenuesAction, VenuesEffect> {

    override fun invoke(action: VenuesAction): Flow<VenuesEffect> {
        return when (action) {
            is VenuesAction.MapMoved -> loadVenuesEffect(LatLon(action.latitude, action.longitude), action.mode)
            is VenuesAction.NavigateBack -> flow {
                emit(VenuesEffect.NavigateBack)
                emitAll(loadVenuesEffect(LatLon(action.latitude, action.longitude), ScreenMode.Exploration))
            }
            is VenuesAction.SelectVenue -> flowOf(VenuesEffect.SelectVenue(action.venue))
            VenuesAction.RequestLocation -> requestLocationEffect()
            is VenuesAction.RetryLoadVenues -> loadVenuesEffect(action.location, ScreenMode.Exploration)
            is VenuesAction.LoadVenuePhoto -> {
                loadVenuesPhotoEffect(action.currentVenues, action.firstVisiblePosition, action.lastVisiblePosition)
            }
        }
    }

    private fun requestLocationEffect(): Flow<VenuesEffect> {
        return flow {
            emit(locationRepository.getDeviceLocation())
        }
            .map { VenuesEffect.DisplayInitialLocation(it) }
            .catch<VenuesEffect> { error ->
                val effect = when (error) {
                    is GoogleServicesUnavailableError -> VenuesEffect.DisplayGoogleServicesUnavailable(error.code)
                    is LocationDisabledError -> {
                        VenuesEffect.DisplayLocationDisabled(error.throwable as? ResolvableApiException)
                    }
                    else -> VenuesEffect.DisplayLocationUnavailable
                }
                emit(effect)
            }
    }

    private fun loadVenuesEffect(
        location: LatLon,
        mode: ScreenMode
    ): Flow<VenuesEffect> {
        return when (mode) {
            ScreenMode.Exploration -> flow {
                emit(VenuesEffect.DisplayVenuesLoading)
                val venues = venueRepository.fetchVenues(location)
                emit(VenuesEffect.DisplayVenues(venues))
            }
            is ScreenMode.Navigation -> flowOf(VenuesEffect.Empty)
        }
            .catch { throwable ->
                when (throwable) {
                    is NetworkNotAvailableError, is ConnectException, is UnknownHostException -> {
                        emit(VenuesEffect.DisplayNetworkNotAvailable(location))
                    }
                    else -> emit(VenuesEffect.DisplayVenuesLoadingFailed(location))
                }
            }
    }

    private val idListForLoading = Collections.synchronizedList<String>(mutableListOf())

    private fun loadVenuesPhotoEffect(
        currentVenues: List<Venue>,
        firstVisiblePosition: Int,
        lastVisiblePosition: Int
    ): Flow<VenuesEffect> {
        if (firstVisiblePosition < 0 || lastVisiblePosition < 0 || currentVenues.isEmpty()) {
            return flowOf(VenuesEffect.Empty)
        }

        val venueIdsToLoadImage = currentVenues.subList(firstVisiblePosition, lastVisiblePosition.inc())
            .filter { it.photo == null && !idListForLoading.contains(it.id) }
            .map { it.id }
        idListForLoading.addAll(venueIdsToLoadImage)

        return venueIdsToLoadImage.asFlow()
            .flatMapMerge { id -> flowOf(venueRepository.fetchVenuePhoto(id) to id) }
            .onEach { (_, id) -> idListForLoading.remove(id) }
            .map { (photo, id) -> photo?.let { VenuesEffect.DisplayVenuePhoto(id, it) } ?: VenuesEffect.Empty }
            .catch { emit(VenuesEffect.Empty) }
    }
}
