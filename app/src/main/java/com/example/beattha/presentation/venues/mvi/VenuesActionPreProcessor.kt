package com.example.beattha.presentation.venues.mvi

import com.example.beattha.mvi.ActionPreProcessor
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import javax.inject.Inject

class VenuesActionPreProcessor @Inject constructor() : ActionPreProcessor<VenuesAction> {
    override fun invoke(actionFlow: Flow<VenuesAction>): Flow<VenuesAction> {
        return actionFlow.distinctUntilChanged { old, new ->
            if (old is VenuesAction.LoadVenuePhoto && new is VenuesAction.LoadVenuePhoto) {
                old.lastVisiblePosition == new.lastVisiblePosition
            } else {
                false
            }
        }
    }
}
