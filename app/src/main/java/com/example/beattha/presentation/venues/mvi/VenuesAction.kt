package com.example.beattha.presentation.venues.mvi

import com.example.beattha.presentation.venues.ScreenMode
import com.example.domain.location.LatLon
import com.example.domain.venue.Venue

sealed class VenuesAction {
    data class NavigateBack(val latitude: Double, val longitude: Double) : VenuesAction()
    data class SelectVenue(val venue: Venue) : VenuesAction()
    data class MapMoved(val latitude: Double, val longitude: Double, val mode: ScreenMode) : VenuesAction()
    object RequestLocation : VenuesAction()
    data class RetryLoadVenues(val location: LatLon) : VenuesAction()
    data class LoadVenuePhoto(
        val currentVenues: List<Venue>,
        val firstVisiblePosition: Int,
        val lastVisiblePosition: Int
    ) : VenuesAction()
}
