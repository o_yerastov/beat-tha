package com.example.beattha.presentation.venues

import com.example.domain.venue.Venue

sealed class ScreenMode {
    object Exploration : ScreenMode()
    data class Navigation(val venue: Venue) : ScreenMode()
}
