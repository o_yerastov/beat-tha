package com.example.beattha.presentation.venues.mvi

import com.example.beattha.mvi.Event
import com.example.domain.location.LatLon
import com.google.android.gms.common.api.ResolvableApiException

sealed class VenuesEvent : Event {
    data class DisplayInitialLocation(val location: LatLon) : VenuesEvent()
    object DisplayLocationUnavailable : VenuesEvent()
    data class DisplayGoogleServicesUnavailable(val code: Int) : VenuesEvent()
    data class MoveCamera(val location: LatLon) : VenuesEvent()
    data class DisplayVenuesLoadingFailed(val location: LatLon) : VenuesEvent()
    data class DisplayLocationDisabled(val resolvableApiException: ResolvableApiException?) : VenuesEvent()
    data class DisplayNetworkNotAvailable(val location: LatLon) : VenuesEvent()
}
