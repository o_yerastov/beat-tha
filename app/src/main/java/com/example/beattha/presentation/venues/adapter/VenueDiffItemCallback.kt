package com.example.beattha.presentation.venues.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.domain.venue.Venue

class VenueDiffItemCallback : DiffUtil.ItemCallback<Venue>() {

    override fun areItemsTheSame(oldItem: Venue, newItem: Venue): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Venue, newItem: Venue): Boolean {
        return oldItem.photo == newItem.photo
    }

    override fun getChangePayload(oldItem: Venue, newItem: Venue): Any? {
        return if (oldItem.photo != newItem.photo && newItem.photo != null) Payload(newItem.photo!!) else null
    }

    inner class Payload(
        val photo: String
    )
}
