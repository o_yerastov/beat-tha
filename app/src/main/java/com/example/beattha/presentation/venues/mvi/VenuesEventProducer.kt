package com.example.beattha.presentation.venues.mvi

import com.example.beattha.mvi.Event
import com.example.beattha.mvi.EventProducer
import javax.inject.Inject

internal class VenuesEventProducer @Inject constructor() : EventProducer<VenuesEffect> {

    override fun invoke(effect: VenuesEffect): Event? {
        return when (effect) {
            is VenuesEffect.DisplayInitialLocation -> VenuesEvent.DisplayInitialLocation(effect.location)
            VenuesEffect.DisplayLocationUnavailable -> VenuesEvent.DisplayLocationUnavailable
            is VenuesEffect.DisplayGoogleServicesUnavailable -> VenuesEvent.DisplayGoogleServicesUnavailable(effect.code)
            is VenuesEffect.SelectVenue -> effect.venue.location?.let(VenuesEvent::MoveCamera)
            is VenuesEffect.DisplayVenuesLoadingFailed -> VenuesEvent.DisplayVenuesLoadingFailed(effect.location)
            is VenuesEffect.DisplayLocationDisabled -> VenuesEvent.DisplayLocationDisabled(effect.resolvableApiException)
            is VenuesEffect.DisplayNetworkNotAvailable -> VenuesEvent.DisplayNetworkNotAvailable(effect.location)
            else -> null
        }
    }
}
