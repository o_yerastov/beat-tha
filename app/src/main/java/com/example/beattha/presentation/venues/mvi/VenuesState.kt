package com.example.beattha.presentation.venues.mvi

import com.example.beattha.presentation.venues.ScreenMode
import com.example.domain.venue.Venue

data class VenuesState(
    val venues: List<Venue>,
    val mode: ScreenMode,
    val loading: Boolean,
) {
    companion object {
        val Initial = VenuesState(emptyList(), ScreenMode.Exploration, false)
    }

}
