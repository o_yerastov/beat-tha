package com.example.beattha.presentation.venues.mvi

import com.example.beattha.mvi.Reducer
import com.example.beattha.presentation.venues.ScreenMode
import com.example.domain.venue.Venue
import javax.inject.Inject

internal class VenuesReducer @Inject constructor() : Reducer<VenuesEffect, VenuesState> {

    private var venueList = emptyList<Venue>()

    @OptIn(ExperimentalStdlibApi::class)
    override fun invoke(currentState: VenuesState, effect: VenuesEffect): VenuesState {
        return when (effect) {
            is VenuesEffect.DisplayVenues -> {
                venueList = effect.venues
                currentState.copy(venues = effect.venues, loading = false)
            }
            VenuesEffect.NavigateBack -> {
                when (currentState.mode) {
                    is ScreenMode.Navigation -> currentState.copy(venues = venueList, mode = ScreenMode.Exploration)
                    else -> currentState
                }
            }
            is VenuesEffect.SelectVenue -> {
                val newVenuesList = venueList.filterNot { it.id == effect.venue.id }
                currentState.copy(
                    venues = newVenuesList,
                    mode = ScreenMode.Navigation(effect.venue)
                )
            }
            VenuesEffect.DisplayVenuesLoading -> {
                venueList = emptyList()
                currentState.copy(venues = venueList, loading = true)
            }
            is VenuesEffect.DisplayVenuePhoto -> {
                processDisplayVenuePhotoEffect(currentState, effect.venueId, effect.photo)
            }
            else -> currentState
        }
    }

    @OptIn(ExperimentalStdlibApi::class)
    private fun processDisplayVenuePhotoEffect(currentState: VenuesState, venueId: String, photo: String): VenuesState {
        val updatedList = buildList<Venue> {
            addAll(venueList)
            indexOfFirst { it.id == venueId }
                .takeIf { it != -1 }
                ?.let { index ->
                    set(index, get(index).copy(photo = photo))
                }
        }
        venueList = updatedList
        val mode = if (currentState.mode is ScreenMode.Navigation) {
            val selectedVenue = currentState.mode.venue
            if (selectedVenue.id == venueId) {
                ScreenMode.Navigation(selectedVenue.copy(photo = photo))
            } else {
                currentState.mode
            }
        } else {
            currentState.mode
        }
        return currentState.copy(venues = updatedList, mode = mode)
    }
}
