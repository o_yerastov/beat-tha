package com.example.beattha.presentation.venues

import com.example.beattha.BaseViewModel
import com.example.beattha.presentation.venues.mvi.VenuesAction
import com.example.beattha.presentation.venues.mvi.VenuesState
import com.example.beattha.presentation.venues.mvi.VenuesStore
import javax.inject.Inject

internal class VenuesViewModel @Inject constructor(
    store: VenuesStore
) : BaseViewModel<VenuesState, VenuesAction>(store) {

    // ViewModel stays clean and it's sole purpose now is to handle configuration changes or accept some
    // data from SavedStateHandle to provide for store during initialization. This gives us a huge benefit that we can test
    // all logic related to screen separately from ViewModel by testing distinct classes like Actor, Reducer etc.
}
