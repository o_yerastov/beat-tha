package com.example.beattha.presentation.venues.adapter

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.beattha.R
import com.example.beattha.databinding.ItemVenueBinding
import com.example.domain.venue.Venue

class VenueAdapter(
    private val onVenueClick: (Venue) -> Unit
) : ListAdapter<Venue, VenueAdapter.VenueHolder>(VenueDiffItemCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueHolder {
        return VenueHolder(ItemVenueBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: VenueHolder, position: Int) {
        holder.bind(currentList[position])
    }

    override fun onBindViewHolder(holder: VenueHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val payload = payloads.last() as VenueDiffItemCallback.Payload
            holder.bindPhoto(payload.photo)
        }
    }

    inner class VenueHolder(private val binding: ItemVenueBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindPhoto(photo: String) = with(binding) {
            image.load(photo)
            ImageViewCompat.setImageTintList(image, null)
        }

        fun bind(venue: Venue) = with(binding) {
            if (venue.photo != null) {
                bindPhoto(venue.photo!!)
            } else {
                image.load(venue.category?.icon)
                ImageViewCompat.setImageTintList(
                    image,
                    ColorStateList.valueOf(ContextCompat.getColor(image.context, R.color.secondary))
                )
            }
            content.setOnClickListener { onVenueClick(venue) }
            name.text = venue.name
            address.text = venue.address
            category.text = venue.category?.name ?: ""
        }
    }
}
