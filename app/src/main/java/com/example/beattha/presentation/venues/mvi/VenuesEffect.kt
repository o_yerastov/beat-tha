package com.example.beattha.presentation.venues.mvi

import com.example.domain.location.LatLon
import com.example.domain.venue.Venue
import com.google.android.gms.common.api.ResolvableApiException

sealed class VenuesEffect {
    object NavigateBack : VenuesEffect()
    object Empty : VenuesEffect()
    data class SelectVenue(val venue: Venue) : VenuesEffect()
    data class DisplayVenues(val venues: List<Venue>) : VenuesEffect()
    data class DisplayInitialLocation(val location: LatLon) : VenuesEffect()
    object DisplayLocationUnavailable : VenuesEffect()
    data class DisplayGoogleServicesUnavailable(val code: Int) : VenuesEffect()
    data class DisplayLocationDisabled(val resolvableApiException: ResolvableApiException?) : VenuesEffect()
    data class DisplayVenuesLoadingFailed(val location: LatLon) : VenuesEffect()
    object DisplayVenuesLoading : VenuesEffect()
    data class DisplayNetworkNotAvailable(val location: LatLon) : VenuesEffect()
    data class DisplayVenuePhoto(val venueId: String, val photo: String) : VenuesEffect()
}
