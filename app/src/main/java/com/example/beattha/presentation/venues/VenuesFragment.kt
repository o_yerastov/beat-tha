package com.example.beattha.presentation.venues

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.content.res.Configuration
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.doOnLayout
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding
import androidx.core.widget.ImageViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.beattha.R
import com.example.beattha.databinding.FragmentVenuesBinding
import com.example.beattha.di.AppInjector
import com.example.beattha.extensions.observe
import com.example.beattha.mvi.Event
import com.example.beattha.presentation.venues.adapter.VenueAdapter
import com.example.beattha.presentation.venues.mvi.VenuesAction
import com.example.beattha.presentation.venues.mvi.VenuesEvent
import com.example.beattha.presentation.venues.mvi.VenuesState
import com.example.domain.location.LatLon
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import kotlin.coroutines.resume
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.inject.Inject
import javax.inject.Provider

class VenuesFragment : Fragment() {

    companion object {
        private const val ZOOM_LEVEL = 12f
    }

    private val locationPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { resultMap ->
            if (resultMap.values.all { it }) {
                viewModel.acceptAction(VenuesAction.RequestLocation)
            } else {
                showLocationPermissionExplanation()
            }
        }
    private val locationResolverLauncher =
        registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                viewLifecycleOwner.lifecycleScope.launch {
                    viewModel.acceptAction(VenuesAction.RequestLocation)
                }
            }
        }

    @Inject
    internal lateinit var vmProvider: Provider<VenuesViewModel>
    private val viewModel by viewModels<VenuesViewModel>({ this }, {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return vmProvider.get() as T
            }
        }
    })

    private var selectedVenueMarker: Marker? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AppInjector.getVenuesComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentVenuesBinding.inflate(inflater, container, false)
        initViewLifecycle(viewLifecycleOwner.lifecycle, savedInstanceState, binding)
        return binding.root
    }

    private fun initViewLifecycle(lifecycle: Lifecycle, savedInstanceState: Bundle?, binding: FragmentVenuesBinding) {
        lifecycle.addObserver(object : DefaultLifecycleObserver {

            override fun onCreate(owner: LifecycleOwner) {
                if (savedInstanceState == null) {
                    checkLocationPermissions()
                }
                val venueAdapter = VenueAdapter { venue ->
                    viewModel.acceptAction(VenuesAction.SelectVenue(venue))
                }
                initViews(venueAdapter, binding)
                handleWindowInsets(binding)

                owner.lifecycleScope.launch {
                    val googleMap = initMap()
                    binding.card.doOnLayout { view ->
                        if (isLandscape()) {
                            val navBarInset = ViewCompat.getRootWindowInsets(requireActivity().window.decorView)
                                ?.getInsets(WindowInsetsCompat.Type.navigationBars())
                                ?.right ?: 0
                            googleMap.setPadding(view.width, 0, navBarInset, 0)
                        } else {
                            googleMap.setPadding(0, 0, 0, view.height)
                        }
                    }
                    val backPressedCallback = OnBackPressedCallbackWithMaps(googleMap)
                    requireActivity().onBackPressedDispatcher.addCallback(owner, backPressedCallback)
                    owner.observe(viewModel.events) { onEvent(it, binding, googleMap) }
                    owner.observe(viewModel.state) { render(it, venueAdapter, googleMap, backPressedCallback, binding) }
                }
            }
        })
    }

    private fun initViews(venueAdapter: VenueAdapter, binding: FragmentVenuesBinding) = with(binding) {
        with(venueList) {
            val layoutManager = LinearLayoutManager(context)
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    viewModel.acceptAction(
                        VenuesAction.LoadVenuePhoto(
                            venueAdapter.currentList,
                            layoutManager.findFirstVisibleItemPosition(),
                            layoutManager.findLastVisibleItemPosition()
                        )
                    )
                }
            })
            this.layoutManager = layoutManager
            setHasFixedSize(true)
            adapter = venueAdapter
        }
        userMarker.doOnLayout { view ->
            userMarker.updateLayoutParams<ViewGroup.MarginLayoutParams> { bottomMargin = view.height / 2 }
        }
    }

    private fun checkLocationPermissions() {
        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED -> {
                viewModel.acceptAction(VenuesAction.RequestLocation)
            }
            shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)
                || shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) -> {
                showLocationPermissionExplanation()
            }
            else -> {
                locationPermissionLauncher.launch(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                )
            }
        }
    }

    private fun showLocationPermissionExplanation() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.location_premissions_title)
            .setMessage(R.string.location_premissions_message)
            .setPositiveButton(R.string.grant_permissions) { _, _ ->
                locationPermissionLauncher.launch(
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    )
                )
            }
            .setNegativeButton(R.string.cancel, null)
            .show()
    }

    private fun handleWindowInsets(binding: FragmentVenuesBinding) = with(binding) {
        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            val statusBarHeight = insets.getInsets(WindowInsetsCompat.Type.statusBars()).top
            val navigationBarHeight = insets.getInsets(WindowInsetsCompat.Type.navigationBars()).let { insets ->
                if (isLandscape()) insets.right else insets.bottom
            }

            venueDetails.updatePadding(top = statusBarHeight, right = if (isLandscape()) navigationBarHeight else 0)
            venueList.updatePadding(bottom = navigationBarHeight, top = if (isLandscape()) statusBarHeight else 0)
            progress.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                bottomMargin = if (isLandscape()) 0 else navigationBarHeight
            }
            textErrorLoading.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                bottomMargin = if (isLandscape()) 0 else navigationBarHeight
            }
            textNoVenues.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                bottomMargin = if (isLandscape()) 0 else navigationBarHeight
            }
            insets
        }
    }

    private suspend fun initMap(): GoogleMap {
        return suspendCancellableCoroutine { continuation ->
            val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
            mapFragment.getMapAsync { map ->
                map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.map_style))
                map.uiSettings.isZoomControlsEnabled = true
                continuation.resume(map)
            }
        }
    }

    private fun onEvent(event: Event, binding: FragmentVenuesBinding, googleMap: GoogleMap) {
        when (event) {
            is VenuesEvent.DisplayInitialLocation -> {
                googleMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(event.location.latitude, event.location.longitude),
                        ZOOM_LEVEL
                    )
                )
            }
            VenuesEvent.DisplayLocationUnavailable -> {
                Snackbar.make(
                    requireActivity().findViewById(android.R.id.content),
                    R.string.failed_to_receive_location,
                    Snackbar.LENGTH_SHORT
                ).show()
            }
            is VenuesEvent.DisplayGoogleServicesUnavailable -> {
                GoogleApiAvailability.getInstance().showErrorDialogFragment(requireActivity(), event.code, 1)
            }
            is VenuesEvent.MoveCamera -> {
                googleMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(event.location.latitude, event.location.longitude),
                        ZOOM_LEVEL
                    )
                )
            }
            is VenuesEvent.DisplayVenuesLoadingFailed -> {
                showLoadingError(R.string.venues_loading_error, event.location, binding)
            }
            is VenuesEvent.DisplayNetworkNotAvailable -> {
                showLoadingError(R.string.network_availability_error, event.location, binding)
            }
            is VenuesEvent.DisplayLocationDisabled -> resolveLocationDisabled(event.resolvableApiException)
        }
    }

    private fun showLoadingError(textRes: Int, location: LatLon, binding: FragmentVenuesBinding) = with(binding) {
        progress.isVisible = false
        textErrorLoading.text = getString(textRes)
        loadingErrorGroup.isVisible = true
        buttonRetry.setOnClickListener {
            viewModel.acceptAction(VenuesAction.RetryLoadVenues(location))
        }
    }

    @SuppressLint("WrongConstant")
    private fun resolveLocationDisabled(exception: ResolvableApiException?) {
        val resolution = exception?.status?.resolution
        if (resolution != null) {
            resolution.let { intent ->
                locationResolverLauncher.launch(
                    IntentSenderRequest.Builder(intent)
                        .setFlags(0, 0)
                        .setFillInIntent(null)
                        .build()
                )
            }
        } else {
            MaterialAlertDialogBuilder(requireContext())
                .setTitle(R.string.location_disabled_title)
                .setMessage(R.string.location_disabled_message)
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.enable) { _, _ ->
                    requireContext().startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
                .show()
        }
    }

    private fun render(
        state: VenuesState,
        adapter: VenueAdapter,
        googleMap: GoogleMap,
        onBackPressedCallback: OnBackPressedCallbackWithMaps,
        binding: FragmentVenuesBinding
    ) = with(binding) {
        onBackPressedCallback.isEnabled = state.mode is ScreenMode.Navigation
        adapter.submitList(state.venues)
        googleMap.setOnCameraIdleListener {
            val location = googleMap.cameraPosition.target
            viewModel.acceptAction(VenuesAction.MapMoved(location.latitude, location.longitude, state.mode))
        }
        if (state.loading || state.venues.isNotEmpty()) {
            loadingErrorGroup.isVisible = false
        }
        textNoVenues.isVisible = !state.loading && state.venues.isEmpty()
        progress.isVisible = state.loading
        when (state.mode) {
            ScreenMode.Exploration -> {
                selectedVenueMarker?.remove()
                selectedVenueMarker = null
                venueDetails.isVisible = false
                userMarker.isVisible = true
            }
            is ScreenMode.Navigation -> {
                userMarker.isVisible = false
                state.mode.venue.location?.let { addVenueMarker(it, googleMap) }

                venueDetails.isVisible = true
                val venue = state.mode.venue
                if (venue.photo != null) {
                    image.load(venue.photo)
                    ImageViewCompat.setImageTintList(image, null)
                } else {
                    image.load(venue.category?.icon)
                    ImageViewCompat.setImageTintList(
                        image,
                        ColorStateList.valueOf(ContextCompat.getColor(image.context, R.color.secondary))
                    )
                }
                name.text = venue.name
                category.text = venue.category?.name ?: ""
                address.text = venue.address
                distance.text = getString(R.string.template_distance, venue.distance)
            }
        }
    }

    private fun addVenueMarker(location: LatLon, googleMap: GoogleMap) {
        fun addMarker() {
            selectedVenueMarker =
                googleMap.addMarker(MarkerOptions().position(LatLng(location.latitude, location.longitude)))
        }

        selectedVenueMarker?.let { currentMarker ->
            if (currentMarker.position.latitude != location.latitude
                || currentMarker.position.longitude != location.longitude
            ) {
                currentMarker.remove()
                addMarker()
            }
        } ?: run {
            addMarker()
        }
    }

    private fun isLandscape(): Boolean {
        return resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
    }

    private inner class OnBackPressedCallbackWithMaps(
        private val googleMap: GoogleMap
    ) : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            val location = googleMap.cameraPosition.target
            viewModel.acceptAction(VenuesAction.NavigateBack(location.latitude, location.longitude))
        }
    }
}
