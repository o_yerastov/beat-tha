package com.example.beattha.presentation.venues.mvi

import com.example.beattha.mvi.Store
import javax.inject.Inject

internal class VenuesStore @Inject constructor(
    actor: VenuesActor,
    reducer: VenuesReducer,
    venuesEventProducer: VenuesEventProducer,
    venuesActionPreProcessor: VenuesActionPreProcessor,
) : Store<VenuesState, VenuesEffect, VenuesAction>(
    VenuesState.Initial,
    actor,
    reducer,
    venuesEventProducer,
    venuesActionPreProcessor
)
