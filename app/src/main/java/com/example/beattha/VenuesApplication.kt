package com.example.beattha

import android.app.Application
import android.util.Log
import com.example.beattha.di.AppInjector

class VenuesApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initDi()
    }

    private fun initDi() {
        AppInjector.init(this)
    }
}
