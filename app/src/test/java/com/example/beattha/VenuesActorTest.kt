package com.example.beattha

import app.cash.turbine.test
import com.example.beattha.presentation.venues.ScreenMode
import com.example.beattha.presentation.venues.mvi.VenuesAction
import com.example.beattha.presentation.venues.mvi.VenuesActor
import com.example.beattha.presentation.venues.mvi.VenuesEffect
import com.example.domain.NetworkNotAvailableError
import com.example.domain.location.GoogleServicesUnavailableError
import com.example.domain.location.LatLon
import com.example.domain.location.LocationDisabledError
import com.example.domain.location.LocationRepository
import com.example.domain.location.LocationUnavailableError
import com.example.domain.venue.Venue
import com.example.domain.venue.VenueRepository
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.common.api.Status
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

@OptIn(ExperimentalTime::class)
class VenuesActorTest {

    private val venueRepository = mockk<VenueRepository>()
    private val locationRepository = mockk<LocationRepository>()
    private val actor = VenuesActor(venueRepository, locationRepository)

    @Test
    fun `should load venues for MapMoved action and Exploration mode`() {
        runBlocking {
            val venues = getVenues()
            coEvery { venueRepository.fetchVenues(any()) } returns venues

            actor(VenuesAction.MapMoved(42.0, 42.0, ScreenMode.Exploration))
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenuesLoading)
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenues(venues))
                    expectComplete()
                }

            coVerify { venueRepository.fetchVenues(LatLon(42.0, 42.0)) }
        }
    }

    @Test
    fun `should not load venues for MapMoved action and Navigation mode`() {
        runBlocking {
            val venues = getVenues()
            coEvery { venueRepository.fetchVenues(any()) } returns venues

            actor(VenuesAction.MapMoved(42.0, 42.0, ScreenMode.Navigation(venues[0])))
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesEffect.Empty)
                    expectComplete()
                }
            coVerify(exactly = 0) { venueRepository.fetchVenues(any()) }
        }
    }

    @Test
    fun `should emit DisplayVenuesLoadingFailed effect when load venues failed`() {
        runBlocking {
            coEvery { venueRepository.fetchVenues(any()) } throws IllegalStateException("error")

            actor(VenuesAction.MapMoved(42.0, 42.0, ScreenMode.Exploration))
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenuesLoading)
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenuesLoadingFailed(LatLon(42.0, 42.0)))
                    expectComplete()
                }
            coVerify { venueRepository.fetchVenues(any()) }
        }
    }

    @Test
    fun `should emit DisplayNetworkNotAvailable effect when received NetworkNotAvailableError`() {
        runBlocking {
            coEvery { venueRepository.fetchVenues(any()) } throws NetworkNotAvailableError()

            actor(VenuesAction.MapMoved(42.0, 42.0, ScreenMode.Exploration))
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenuesLoading)
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayNetworkNotAvailable(LatLon(42.0, 42.0)))
                    expectComplete()
                }
            coVerify { venueRepository.fetchVenues(any()) }
        }
    }

    @Test
    fun `should load venues for RetryLoadVenues action`() {
        runBlocking {
            val venues = getVenues()
            coEvery { venueRepository.fetchVenues(any()) } returns venues

            actor(VenuesAction.RetryLoadVenues(LatLon(42.0, 42.0)))
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenuesLoading)
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenues(venues))
                    expectComplete()
                }

            coVerify { venueRepository.fetchVenues(LatLon(42.0, 42.0)) }
        }
    }

    @Test
    fun `should load venues for NavigateBack action`() {
        runBlocking {
            val venues = getVenues()
            coEvery { venueRepository.fetchVenues(any()) } returns venues

            actor(VenuesAction.NavigateBack(42.0, 42.0))
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesEffect.NavigateBack)
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenuesLoading)
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenues(venues))
                    expectComplete()
                }

            coVerify { venueRepository.fetchVenues(LatLon(42.0, 42.0)) }
        }
    }

    @Test
    fun `should emit SelectVenue for SelectVenue action`() {
        runBlocking {
            val venue = getVenues()[0]
            actor(VenuesAction.SelectVenue(venue))
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesEffect.SelectVenue(venue))
                    expectComplete()
                }
        }
    }

    @Test
    fun `should request location from repository for RequestLocation action`() {
        runBlocking {
            coEvery { locationRepository.getDeviceLocation() } returns LatLon(42.0, 42.0)
            actor(VenuesAction.RequestLocation)
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayInitialLocation(LatLon(42.0, 42.0)))
                    expectComplete()
                }
        }
    }

    @Test
    fun `should emit DisplayGoogleServicesUnavailable effect for error`() {
        runBlocking {
            coEvery { locationRepository.getDeviceLocation() } throws GoogleServicesUnavailableError(42)
            actor(VenuesAction.RequestLocation)
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayGoogleServicesUnavailable(42))
                    expectComplete()
                }
        }
    }

    @Test
    fun `should emit DisplayLocationDisabled effect for error`() {
        val exception = ResolvableApiException(Status(1))
        runBlocking {
            coEvery { locationRepository.getDeviceLocation() } throws LocationDisabledError(exception)
            actor(VenuesAction.RequestLocation)
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayLocationDisabled(exception))
                    expectComplete()
                }
        }
    }

    @Test
    fun `should emit DisplayLocationUnavailable effect for error`() {
        runBlocking {
            coEvery { locationRepository.getDeviceLocation() } throws LocationUnavailableError()
            actor(VenuesAction.RequestLocation)
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayLocationUnavailable)
                    expectComplete()
                }
        }
    }

    @Nested
    inner class LoadVenuePhotoEffect {
        @Test
        fun `should emit Empty effect when firstVisiblePosition is less than 0`() {
            runBlocking {
                actor(VenuesAction.LoadVenuePhoto(listOf(getVenues().first()), -1, 2))
                    .test {
                        assertThat(expectItem()).isEqualTo(VenuesEffect.Empty)
                        expectComplete()
                    }
            }
        }

        @Test
        fun `should emit Empty effect when lastVisiblePosition is less than 0`() {
            runBlocking {
                actor(VenuesAction.LoadVenuePhoto(listOf(getVenues().first()), 3, -1))
                    .test {
                        assertThat(expectItem()).isEqualTo(VenuesEffect.Empty)
                        expectComplete()
                    }
            }
        }

        @Test
        fun `should emit Empty effect when currentVenues are empty`() {
            runBlocking {
                actor(VenuesAction.LoadVenuePhoto(emptyList(), 3, 6))
                    .test {
                        assertThat(expectItem()).isEqualTo(VenuesEffect.Empty)
                        expectComplete()
                    }
            }
        }

        @Test
        fun `should load photos only for first and last visible positions`() {
            runBlocking {
                coEvery { venueRepository.fetchVenuePhoto(any()) } returns "photo"

                actor(VenuesAction.LoadVenuePhoto(getVenues(), 0, 2))
                    .test {
                        assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenuePhoto("1", "photo"))
                        assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenuePhoto("2", "photo"))
                        assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenuePhoto("3", "photo"))
                        expectComplete()
                    }
                coVerify { venueRepository.fetchVenuePhoto("1") }
                coVerify { venueRepository.fetchVenuePhoto("2") }
                coVerify { venueRepository.fetchVenuePhoto("3") }
            }
        }

        @OptIn(ExperimentalStdlibApi::class)
        @Test
        fun `should not load photos for venues that have photo`() {
            runBlocking {
                coEvery { venueRepository.fetchVenuePhoto(any()) } returns "photo"

                val venues = buildList<Venue> {
                    addAll(getVenues())
                    set(0, get(0).copy(photo = "photo1"))
                    set(1, get(1).copy(photo = "photo2"))
                }
                actor(VenuesAction.LoadVenuePhoto(venues, 0, 2))
                    .test {
                        assertThat(expectItem()).isEqualTo(VenuesEffect.DisplayVenuePhoto("3", "photo"))
                        expectComplete()
                    }
                coVerify(exactly = 1) { venueRepository.fetchVenuePhoto("3") }
            }
        }

        @OptIn(ExperimentalStdlibApi::class)
        @Test
        fun `should emit Empty effect when error occurred`() {
            runBlocking {
                coEvery { venueRepository.fetchVenuePhoto(any()) } throws IllegalStateException()

                actor(VenuesAction.LoadVenuePhoto(getVenues(), 0, 0))
                    .test {
                        assertThat(expectItem()).isEqualTo(VenuesEffect.Empty)
                        expectComplete()
                    }
                coVerify { venueRepository.fetchVenuePhoto("1") }
            }
        }
    }

    private fun getVenues(): List<Venue> {
        return (1..5).map { index ->
            Venue(index.toString(), index.toString(), emptyList(), index.toString(), null, index, null)
        }
    }
}
