package com.example.beattha

import com.example.beattha.presentation.venues.mvi.VenuesEffect
import com.example.beattha.presentation.venues.mvi.VenuesEvent
import com.example.beattha.presentation.venues.mvi.VenuesEventProducer
import com.example.domain.location.LatLon
import com.example.domain.venue.Venue
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class VenuesEventProducerTest {

    private val producer = VenuesEventProducer()

    @Test
    fun `should map DisplayInitialLocation event`() {
        val event = producer(VenuesEffect.DisplayInitialLocation(LatLon(42.0, 42.0)))

        assertThat(event).isEqualTo(VenuesEvent.DisplayInitialLocation(LatLon(42.0, 42.0)))
    }

    @Test
    fun `should map DisplayLocationUnavailable event`() {
        val event = producer(VenuesEffect.DisplayLocationUnavailable)

        assertThat(event).isEqualTo(VenuesEvent.DisplayLocationUnavailable)
    }

    @Test
    fun `should map DisplayGoogleServicesUnavailable event`() {
        val event = producer(VenuesEffect.DisplayGoogleServicesUnavailable(42))

        assertThat(event).isEqualTo(VenuesEvent.DisplayGoogleServicesUnavailable(42))
    }

    @Test
    fun `should map MoveCamera event if location is not null`() {
        val event = producer(VenuesEffect.SelectVenue(Venue("1", "1", emptyList(), "1", LatLon(42.0, 42.0), 42, null)))

        assertThat(event).isEqualTo(VenuesEvent.MoveCamera(LatLon(42.0, 42.0)))
    }

    @Test
    fun `should not map MoveCamera event if location is absent`() {
        val event = producer(VenuesEffect.SelectVenue(Venue("1", "1", emptyList(), "1", null, 42, null)))

        assertThat(event).isEqualTo(null)
    }

    @Test
    fun `should map DisplayVenuesLoadingFailed event`() {
        val event = producer(VenuesEffect.DisplayVenuesLoadingFailed(LatLon(42.0, 42.0)))

        assertThat(event).isEqualTo(VenuesEvent.DisplayVenuesLoadingFailed(LatLon(42.0, 42.0)))
    }

    @Test
    fun `should map DisplayLocationDisabled event`() {
        val event = producer(VenuesEffect.DisplayLocationDisabled(null))

        assertThat(event).isEqualTo(VenuesEvent.DisplayLocationDisabled(null))
    }

    @Test
    fun `should map DisplayNetworkNotAvailable event`() {
        val event = producer(VenuesEffect.DisplayNetworkNotAvailable(LatLon(42.0, 42.0)))

        assertThat(event).isEqualTo(VenuesEvent.DisplayNetworkNotAvailable(LatLon(42.0, 42.0)))
    }

    @Test
    fun `should not map DisplayVenuesLoading event`() {
        val event = producer(VenuesEffect.DisplayVenuesLoading)

        assertThat(event).isNull()
    }

    @Test
    fun `should not map DisplayVenues event`() {
        val event = producer(VenuesEffect.DisplayVenues(emptyList()))

        assertThat(event).isNull()
    }

    @Test
    fun `should not map NavigateBack event`() {
        val event = producer(VenuesEffect.NavigateBack)

        assertThat(event).isNull()
    }

    @Test
    fun `should not map Empty event`() {
        val event = producer(VenuesEffect.Empty)

        assertThat(event).isNull()
    }

    @Test
    fun `should not map DisplayVenuePhoto event`() {
        val event = producer(VenuesEffect.DisplayVenuePhoto("1", "1"))

        assertThat(event).isNull()
    }

}
