package com.example.beattha

import com.example.beattha.presentation.venues.ScreenMode
import com.example.beattha.presentation.venues.mvi.VenuesEffect
import com.example.beattha.presentation.venues.mvi.VenuesReducer
import com.example.beattha.presentation.venues.mvi.VenuesState
import com.example.domain.location.LatLon
import com.example.domain.venue.Venue
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class VenuesReducerTest {

    private val reducer = VenuesReducer()

    @Test
    fun `should handle DisplayVenuesLoading effect`() {
        val state = reducer(VenuesState.Initial, VenuesEffect.DisplayVenuesLoading)

        assertThat(state.loading).isTrue()
        assertThat(state.venues.isEmpty())
    }

    @Test
    fun `should handle DisplayVenues effect`() {
        val venues = getVenues()
        val state = reducer(VenuesState.Initial, VenuesEffect.DisplayVenues(venues))

        assertThat(state.loading).isFalse()
        assertThat(state.venues).containsExactlyElementsOf(venues)
    }

    @Test
    fun `should handle SelectVenue effect`() {
        val venues = getVenues()
        val state = reducer(VenuesState.Initial, VenuesEffect.SelectVenue(venues[3]))

        assertThat(state.venues).doesNotContain(venues[3])
        assertThat(state.mode).isEqualTo(ScreenMode.Navigation(venues[3]))
    }

    @Test
    fun `should handle NavigateBack effect for Navigation mode`() {
        val venues = getVenues()
        reducer(VenuesState.Initial, VenuesEffect.DisplayVenues(venues))
        val state =
            reducer(VenuesState.Initial.copy(mode = ScreenMode.Navigation(venues[0])), VenuesEffect.NavigateBack)

        assertThat(state.venues).containsExactlyElementsOf(venues)
        assertThat(state.mode).isEqualTo(ScreenMode.Exploration)
    }

    @Test
    fun `should handle NavigateBack effect for Exploration mode`() {
        val venues = getVenues()
        reducer(VenuesState.Initial, VenuesEffect.DisplayVenues(venues))
        val state = reducer(VenuesState.Initial.copy(mode = ScreenMode.Exploration), VenuesEffect.NavigateBack)

        assertThat(state).isEqualTo(VenuesState.Initial.copy(mode = ScreenMode.Exploration))
    }

    @Test
    fun `should update photo only for venue in list for DisplayVenuePhoto effect for Exploration mode`() {
        val venues = getVenues()

        assertThat(venues.find { it.id == "3" }!!.photo).isNull()

        reducer(VenuesState.Initial, VenuesEffect.DisplayVenues(venues))
        val state =
            reducer(VenuesState.Initial.copy(mode = ScreenMode.Exploration), VenuesEffect.DisplayVenuePhoto("3", "3"))

        assertThat(state.venues.find { it.id == "3" }!!.photo).isEqualTo("3")
        assertThat(state.mode).isEqualTo(ScreenMode.Exploration)
    }

    @Test
    fun `should update photo for selected venue for DisplayVenuePhoto effect for Navigation mode if venues are the same`() {
        val venues = getVenues()

        val selectedVenue = venues.find { it.id == "3" }!!

        assertThat(selectedVenue.photo).isNull()

        reducer(VenuesState.Initial, VenuesEffect.DisplayVenues(venues))
        val state = reducer(
            VenuesState.Initial.copy(mode = ScreenMode.Navigation(selectedVenue)),
            VenuesEffect.DisplayVenuePhoto("3", "3")
        )

        assertThat(state.mode).isEqualTo(ScreenMode.Navigation(selectedVenue.copy(photo = "3")))
    }

    @Test
    fun `should not update photo for selected venue for DisplayVenuePhoto effect for Navigation mode if venues are different`() {
        val venues = getVenues()

        val selectedVenue = venues.find { it.id == "3" }!!

        assertThat(selectedVenue.photo).isNull()

        reducer(VenuesState.Initial, VenuesEffect.DisplayVenues(venues))
        val state = reducer(
            VenuesState.Initial.copy(mode = ScreenMode.Navigation(selectedVenue)),
            VenuesEffect.DisplayVenuePhoto("4", "4")
        )

        assertThat(state.mode).isEqualTo(ScreenMode.Navigation(selectedVenue))
    }

    @Test
    fun `should not handle Empty effect`() {
        val state = reducer(VenuesState.Initial, VenuesEffect.Empty)

        assertThat(state).isEqualTo(VenuesState.Initial)
    }

    @Test
    fun `should not handle DisplayInitialLocation effect`() {
        val state = reducer(VenuesState.Initial, VenuesEffect.DisplayInitialLocation(LatLon(42.0, 42.0)))

        assertThat(state).isEqualTo(VenuesState.Initial)
    }

    @Test
    fun `should not handle DisplayLocationUnavailable effect`() {
        val state = reducer(VenuesState.Initial, VenuesEffect.DisplayLocationUnavailable)

        assertThat(state).isEqualTo(VenuesState.Initial)
    }

    @Test
    fun `should not handle DisplayGoogleServicesUnavailable effect`() {
        val state = reducer(VenuesState.Initial, VenuesEffect.DisplayGoogleServicesUnavailable(42))

        assertThat(state).isEqualTo(VenuesState.Initial)
    }

    @Test
    fun `should not handle DisplayLocationDisabled effect`() {
        val state = reducer(VenuesState.Initial, VenuesEffect.DisplayLocationDisabled(null))

        assertThat(state).isEqualTo(VenuesState.Initial)
    }

    @Test
    fun `should not handle DisplayVenuesLoadingFailed effect`() {
        val state = reducer(VenuesState.Initial, VenuesEffect.DisplayVenuesLoadingFailed(LatLon(42.0, 42.0)))

        assertThat(state).isEqualTo(VenuesState.Initial)
    }

    private fun getVenues(): List<Venue> {
        return (1..5).map { index ->
            Venue(index.toString(), index.toString(), emptyList(), index.toString(), null, index, null)
        }
    }
}
