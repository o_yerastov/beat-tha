package com.example.beattha

import app.cash.turbine.test
import com.example.beattha.presentation.venues.mvi.VenuesAction
import com.example.beattha.presentation.venues.mvi.VenuesActionPreProcessor
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

@OptIn(ExperimentalTime::class)
class VenuesActionPreProcessorTest {

    private val preProcessor = VenuesActionPreProcessor()

    @Test
    fun `should distinct LoadVenuePhoto with same lastVisiblePosition`() {
        runBlocking {
            val actions = listOf<VenuesAction>(
                VenuesAction.LoadVenuePhoto(emptyList(), 1, 3),
                VenuesAction.LoadVenuePhoto(emptyList(), 2, 3),
                VenuesAction.LoadVenuePhoto(emptyList(), 3, 3),
            )

            preProcessor(actions.asFlow())
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesAction.LoadVenuePhoto(emptyList(), 1, 3))
                    expectComplete()
                }
        }
    }

    @Test
    fun `should not distinct LoadVenuePhoto with different lastVisiblePosition`() {
        runBlocking {
            val actions = listOf<VenuesAction>(
                VenuesAction.LoadVenuePhoto(emptyList(), 1, 3),
                VenuesAction.LoadVenuePhoto(emptyList(), 2, 4),
                VenuesAction.LoadVenuePhoto(emptyList(), 3, 5),
            )

            preProcessor(actions.asFlow())
                .test {
                    assertThat(expectItem()).isEqualTo(VenuesAction.LoadVenuePhoto(emptyList(), 1, 3))
                    assertThat(expectItem()).isEqualTo(VenuesAction.LoadVenuePhoto(emptyList(), 2, 4))
                    assertThat(expectItem()).isEqualTo(VenuesAction.LoadVenuePhoto(emptyList(), 3, 5))
                    expectComplete()
                }
        }
    }

}
