package com.example.data.venue

import com.example.data.venue.api.VenueApi
import com.example.data.venue.mapper.VenueMapper
import com.example.domain.location.LatLon
import com.example.domain.venue.Venue
import com.example.domain.venue.VenueRepository
import javax.inject.Inject

internal class VenueRepositoryImpl @Inject constructor(
    private val venueApi: VenueApi,
    private val venueMapper: VenueMapper,
) : VenueRepository {

    override suspend fun fetchVenues(latLon: LatLon): List<Venue> {
        return venueApi.getVenues(latLon).response.venues.map(venueMapper::map)
            .sortedBy { it.distance }
    }

    override suspend fun fetchVenuePhoto(id: String): String? {
        return venueApi.getVenuePhoto(id).response.photos.items.firstOrNull()?.let { dto ->
            dto.prefix + "100x100" + dto.suffix
        }
    }
}
