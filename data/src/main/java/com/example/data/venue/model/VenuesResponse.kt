package com.example.data.venue.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class VenuesResponse(
    @SerialName("response") val response: VenuesListResponse
) {
    @Serializable
    data class VenuesListResponse(
        @SerialName("venues") val venues: List<VenueDto>
    )
}
