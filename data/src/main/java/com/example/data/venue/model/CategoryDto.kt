package com.example.data.venue.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class CategoryDto(
    @SerialName("id") val id: String,
    @SerialName("name") val name: String,
    @SerialName("primary") val primary: Boolean,
    @SerialName("icon") val icon: Icon,
) {
    @Serializable
    data class Icon(
        @SerialName("prefix") val prefix: String,
        @SerialName("suffix") val suffix: String,
    )
}
