package com.example.data.venue.api

import com.example.data.venue.model.VenuePhotoResponse
import com.example.data.venue.model.VenuesResponse
import com.example.domain.location.LatLon

internal interface VenueApi {
    suspend fun getVenues(latLon: LatLon): VenuesResponse
    suspend fun getVenuePhoto(id:String): VenuePhotoResponse
}
