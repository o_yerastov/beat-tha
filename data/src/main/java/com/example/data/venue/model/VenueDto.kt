package com.example.data.venue.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class VenueDto(
    @SerialName("id") val id: String,
    @SerialName("name") val name: String,
    @SerialName("categories") val categories: List<CategoryDto>,
    @SerialName("location") val location: Location
) {
    @Serializable
    data class Location(
        @SerialName("formattedAddress") val address: List<String>,
        @SerialName("labeledLatLngs") val labeledLatLngs: List<LabeledLatLng>?,
        @SerialName("distance") val distance: Int,
    )

    @Serializable
    data class LabeledLatLng(
        @SerialName("lat") val latitude: Double,
        @SerialName("lng") val longitude: Double,
    )
}
