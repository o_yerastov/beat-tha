package com.example.data.venue.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class VenuePhotoDto(
    @SerialName("id") val id: String,
    @SerialName("prefix") val prefix: String,
    @SerialName("suffix") val suffix: String,
)
