package com.example.data.venue.mapper

import com.example.data.venue.model.VenueDto
import com.example.domain.location.LatLon
import com.example.domain.venue.Venue
import javax.inject.Inject

internal class VenueMapper @Inject constructor(private val categoryMapper: CategoryMapper) {

    fun map(input: VenueDto): Venue {
        return with(input) {
            val distance = location.distance
            val address = location.address.joinToString(separator = ", ")
            val location = location.labeledLatLngs?.first()

            Venue(
                id,
                name,
                categories.map(categoryMapper::map),
                address,
                location?.let {
                    LatLon(location.latitude, location.longitude)
                },
                distance,
                null
            )
        }
    }
}
