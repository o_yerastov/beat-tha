package com.example.data.venue.mapper

import com.example.data.venue.model.CategoryDto
import com.example.domain.venue.Category
import javax.inject.Inject

internal class CategoryMapper @Inject constructor() {

    fun map(input: CategoryDto): Category {
        return with(input) { Category(id, name, primary, "${icon.prefix}64${icon.suffix}") }
    }
}
