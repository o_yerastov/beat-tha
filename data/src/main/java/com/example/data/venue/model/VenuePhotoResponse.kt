package com.example.data.venue.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class VenuePhotoResponse(
    @SerialName("response") val response: VenuePhotos
) {
    @Serializable
    data class VenuePhotos(
        @SerialName("photos") val photos: VenuePhotosDto
    )
}
