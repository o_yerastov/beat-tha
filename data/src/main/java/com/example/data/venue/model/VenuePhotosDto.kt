package com.example.data.venue.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class VenuePhotosDto(
    @SerialName("items") val items: List<VenuePhotoDto>
)
