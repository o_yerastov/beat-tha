package com.example.data.venue.api

import com.example.data.venue.model.VenuePhotoResponse
import com.example.data.venue.model.VenuesResponse
import com.example.domain.location.LatLon
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import javax.inject.Inject

internal class VenueApiImpl @Inject constructor(
    private val httpClient: HttpClient
) : VenueApi {

    override suspend fun getVenues(latLon: LatLon): VenuesResponse {
        return httpClient.get("venues/search") {
            parameter("ll", "${latLon.latitude},${latLon.longitude}")
            parameter("categoryId", "4d4b7105d754a06374d81259")
        }
    }

    override suspend fun getVenuePhoto(id: String): VenuePhotoResponse {
        return httpClient.get("venues/$id/photos") {
            parameter("limit", 1)
            parameter("group", "venue")
        }
    }
}
