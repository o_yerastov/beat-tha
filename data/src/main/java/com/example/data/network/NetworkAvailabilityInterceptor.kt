package com.example.data.network

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.example.domain.NetworkNotAvailableError
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

internal class NetworkAvailabilityInterceptor @Inject constructor(private val context: Context) : Interceptor {

    private val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isAvailable()) throw NetworkNotAvailableError()

        return chain.proceed(chain.request())
    }

    @Suppress("DEPRECATION")
    @SuppressLint("MissingPermission")
    private fun isAvailable(): Boolean {
        return connectivityManager?.run {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val networkCapabilities = activeNetwork ?: return false
                val actNw = getNetworkCapabilities(networkCapabilities) ?: return false
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
            } else {
                activeNetworkInfo?.run {
                    type == ConnectivityManager.TYPE_WIFI
                        || type == ConnectivityManager.TYPE_MOBILE
                        || type == ConnectivityManager.TYPE_ETHERNET
                } ?: false
            }
        } ?: false
    }
}
