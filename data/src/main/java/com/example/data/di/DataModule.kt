package com.example.data.di

import android.content.Context
import com.example.data.venue.api.VenueApi
import com.example.data.location.LocationProvider
import com.example.data.location.LocationProviderImpl
import com.example.data.location.LocationRepositoryImpl
import com.example.data.venue.VenueRepositoryImpl
import com.example.data.venue.mapper.VenueMapper
import com.example.domain.location.LocationRepository
import com.example.domain.venue.VenueRepository
import dagger.Module
import dagger.Provides

@Module
internal object DataModule {

    @Provides
    fun venueRepository(api: VenueApi, mapper: VenueMapper): VenueRepository {
        return VenueRepositoryImpl(api, mapper)
    }

    @Provides
    fun locationRepository(locationProvider: LocationProvider): LocationRepository {
        return LocationRepositoryImpl(locationProvider)
    }

    @Provides
    fun locationProvider(/*@ApplicationContext */context: Context): LocationProvider {
        return LocationProviderImpl(context)
    }
}
