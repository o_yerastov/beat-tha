package com.example.data.di

import com.example.data.venue.api.VenueApi
import com.example.data.venue.api.VenueApiImpl
import dagger.Binds
import dagger.Module

@Module
internal interface ApiModule {

    @Binds
    fun venueApi(impl: VenueApiImpl): VenueApi
}
