package com.example.data.di

import android.content.Context
import com.example.domain.location.LocationRepository
import com.example.domain.venue.VenueRepository
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [DataModule::class, ApiModule::class, NetworkModule::class]
)
interface DataComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): DataComponent
    }

    val venueRepository: VenueRepository
    val locationRepository: LocationRepository
}
