package com.example.data.di

import android.content.Context
import com.example.data.BuildConfig
import com.example.data.network.NetworkAvailabilityInterceptor
import dagger.Module
import dagger.Provides
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.features.defaultRequest
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.parameter
import io.ktor.http.URLProtocol
import io.ktor.http.takeFrom
import java.io.IOException
import java.time.format.DateTimeFormatter
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@Module
internal object NetworkModule {

    @Singleton
    @Provides
    fun okHttpClient(
        networkAvailabilityInterceptor: NetworkAvailabilityInterceptor,
        httpLoggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(networkAvailabilityInterceptor)
            .apply {
                if (BuildConfig.DEBUG) addInterceptor(httpLoggingInterceptor)
            }
            .build()
    }

    @Singleton
    @Provides
    fun networkAvailabilityInterceptor(context: Context): NetworkAvailabilityInterceptor {
        return NetworkAvailabilityInterceptor(context)
    }

    @Provides
    @Singleton
    fun httpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @Singleton
    fun httpClient(okHttpClient: OkHttpClient): HttpClient {
        return HttpClient(OkHttp) {
            defaultRequest {
                url {
                    protocol = URLProtocol.HTTPS
                    host = BuildConfig.HOST
                    encodedPath = "${BuildConfig.BASE_PATH}$encodedPath"
                }
                parameter("client_id", BuildConfig.CLIENT_ID)
                parameter("client_secret", BuildConfig.CLIENT_SECRET)
                parameter("v", "20210807")
            }
            engine {
                preconfigured = okHttpClient
            }

            install(JsonFeature) {
                val json = Json {
                    ignoreUnknownKeys = true
                }

                serializer = KotlinxSerializer(json)
            }
        }
    }
}
