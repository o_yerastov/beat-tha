package com.example.data.location

import android.annotation.SuppressLint
import android.content.Context
import com.example.domain.location.GoogleServicesUnavailableError
import com.example.domain.location.LatLon
import com.example.domain.location.LocationDisabledError
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.AvailabilityException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.inject.Inject

internal class LocationProviderImpl @Inject constructor(
    private val context: Context
) : LocationProvider {

    // Suppress lint check as we have declared permissions in app module
    @SuppressLint("MissingPermission")
    override suspend fun getLastDeviceLocation(): LatLon? {
        checkLocationServiceEnabled()

        return suspendCancellableCoroutine { continuation ->
            val client = LocationServices.getFusedLocationProviderClient(context)
            GoogleApiAvailability.getInstance()
                .checkApiAvailability(client)
                .addOnFailureListener { exception ->
                    val code = (exception as AvailabilityException).getConnectionResult(client).errorCode
                    continuation.resumeWithException(GoogleServicesUnavailableError(code))
                }
                .onSuccessTask { client.lastLocation }
                .addOnSuccessListener { location ->
                    continuation.resume(location?.let { LatLon(it.latitude, it.longitude) })
                }
                .addOnFailureListener { continuation.resume(null) }
        }
    }

    private suspend fun checkLocationServiceEnabled() {
        val client = LocationServices.getSettingsClient(context)
        val locationSettingsRequest = LocationSettingsRequest.Builder()
            .addLocationRequest(LocationRequest.create().setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY))
            .build()
        return suspendCancellableCoroutine { continuation ->
            client.checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener { continuation.resume(Unit) }
                .addOnFailureListener { exception ->
                    val throwable = exception.takeIf {
                        exception is ResolvableApiException
                            && exception.statusCode == LocationSettingsStatusCodes.RESOLUTION_REQUIRED
                    }
                    continuation.resumeWithException(LocationDisabledError(throwable))
                }
        }

    }
}
