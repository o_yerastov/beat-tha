package com.example.data.location

import com.example.domain.location.LatLon

internal interface LocationProvider {

    suspend fun getLastDeviceLocation(): LatLon?
}
