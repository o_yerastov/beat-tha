package com.example.data.location

import com.example.domain.location.LocationRepository
import com.example.domain.location.LatLon
import com.example.domain.location.LocationUnavailableError
import javax.inject.Inject

internal class LocationRepositoryImpl @Inject constructor(
    private val locationProvider: LocationProvider
) : LocationRepository {

    override suspend fun getDeviceLocation(): LatLon {
        return locationProvider.getLastDeviceLocation() ?: throw LocationUnavailableError()
    }
}
