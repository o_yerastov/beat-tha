package com.example.data.venue

import com.example.data.venue.mapper.CategoryMapper
import com.example.data.venue.model.CategoryDto
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class CategoryMapperTest {

    private val mapper = CategoryMapper()

    @Test
    fun `should map fields without change`(){
        val categoryDto = CategoryDto("1", "1", true, CategoryDto.Icon("1", "1"))

        val category = mapper.map(categoryDto)

        assertThat(category.name).isEqualTo("1")
        assertThat(category.id).isEqualTo("1")
        assertThat(category.primary).isEqualTo(true)
    }

     @Test
    fun `should map correct icon url`(){
        val categoryDto = CategoryDto("1", "1", true, CategoryDto.Icon("1_", ".png"))

        val category = mapper.map(categoryDto)

        assertThat(category.icon).isEqualTo("1_64.png")
    }

}