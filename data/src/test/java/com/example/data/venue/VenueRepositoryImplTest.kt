package com.example.data.venue

import com.example.data.venue.api.VenueApi
import com.example.data.venue.mapper.VenueMapper
import com.example.data.venue.model.VenueDto
import com.example.data.venue.model.VenuePhotoDto
import com.example.data.venue.model.VenuePhotoResponse
import com.example.data.venue.model.VenuePhotosDto
import com.example.data.venue.model.VenuesResponse
import com.example.domain.location.LatLon
import com.example.domain.venue.Venue
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import java.util.function.Predicate
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class VenueRepositoryImplTest {

    private val venueApi = mockk<VenueApi>()
    private val venueMapper = mockk<VenueMapper>()
    private val venueRepository = VenueRepositoryImpl(venueApi, venueMapper)

    @Test
    fun `should return venues sorted by distance ascending`() {
        runBlocking {
            val venueDtos = listOf(
                VenueDto("1", "1", emptyList(), VenueDto.Location(listOf("1"), null, 5)),
                VenueDto("2", "2", emptyList(), VenueDto.Location(listOf("2"), null, 6)),
                VenueDto("3", "3", emptyList(), VenueDto.Location(listOf("3"), null, 1)),
            )
            coEvery { venueApi.getVenues(any()) } returns VenuesResponse(VenuesResponse.VenuesListResponse(venueDtos))
            venueDtos.forEach {
                every { venueMapper.map(it) } returns Venue(
                    it.id,
                    it.name,
                    emptyList(),
                    "",
                    null,
                    it.location.distance,
                    null
                )
            }

            val venues = venueRepository.fetchVenues(LatLon(42.0, 42.0))
            coVerify { venueApi.getVenues(LatLon(42.0, 42.0)) }

            assertThat(Predicate<Boolean> {
                venues[0].id == "3" && venues[1].id == "1" && venues[2].id == "2"
            })
        }
    }

    @Test
    fun `should return photo correctly constructed from photo dto`() {
        coEvery {
            venueApi.getVenuePhoto(any())
        } returns VenuePhotoResponse(VenuePhotoResponse.VenuePhotos(VenuePhotosDto(listOf(VenuePhotoDto("1", "prefix", "suffix")))))

        runBlocking {
            val photo = venueRepository.fetchVenuePhoto("1")

            assertThat(photo).isEqualTo("prefix100x100suffix")
        }
    }
}
