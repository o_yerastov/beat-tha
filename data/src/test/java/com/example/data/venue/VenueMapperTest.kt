package com.example.data.venue

import com.example.data.venue.mapper.CategoryMapper
import com.example.data.venue.mapper.VenueMapper
import com.example.data.venue.model.CategoryDto
import com.example.data.venue.model.VenueDto
import com.example.domain.location.LatLon
import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class VenueMapperTest {

    private val categoryMapper = mockk<CategoryMapper>()
    private val mapper = VenueMapper(categoryMapper)

    @Test
    fun `should join address items separated by comma`() {
        every { categoryMapper.map(any()) } returns mockk()
        val venueDto = VenueDto("1", "1", emptyList(), VenueDto.Location(listOf("country", "city", "street"), null, 42))

        val venue = mapper.map(venueDto)

        assertThat(venue.address).isEqualTo("country, city, street")
    }

    @Test
    fun `should take first location from list`() {
        every { categoryMapper.map(any()) } returns mockk()
        val venueDto = VenueDto(
            "1",
            "1",
            emptyList(),
            VenueDto.Location(
                listOf("address"),
                listOf(VenueDto.LabeledLatLng(31.0, 31.0), VenueDto.LabeledLatLng(42.0, 42.0)),
                42
            )
        )

        val venue = mapper.map(venueDto)

        assertThat(venue.location).isEqualTo(LatLon(31.0, 31.0))
    }

    @Test
    fun `should map fields without changing`() {
        every { categoryMapper.map(any()) } returns mockk()
        val venueDto = VenueDto(
            "1",
            "1",
            emptyList(),
            VenueDto.Location(
                listOf("address"),
                listOf(VenueDto.LabeledLatLng(31.0, 31.0), VenueDto.LabeledLatLng(42.0, 42.0)),
                42
            )
        )

        val venue = mapper.map(venueDto)

        assertThat(venue.name).isEqualTo("1")
        assertThat(venue.id).isEqualTo("1")
        assertThat(venue.distance).isEqualTo(venueDto.location.distance)
    }

    @Test
    fun `should map same amount of categories`() {
        every { categoryMapper.map(any()) } returns mockk()
        val venueDto = VenueDto(
            "1",
            "1",
            listOf(
                CategoryDto("1", "1", true, CategoryDto.Icon("1", "1")),
                CategoryDto("2", "2", false, CategoryDto.Icon("1", "1")),
                CategoryDto("3", "3", false, CategoryDto.Icon("1", "1")),
            ),
            VenueDto.Location(
                listOf("address"),
                listOf(VenueDto.LabeledLatLng(31.0, 31.0), VenueDto.LabeledLatLng(42.0, 42.0)),
                42
            )
        )

        val venue = mapper.map(venueDto)

        assertThat(venue.categories.size).isEqualTo(3)
    }
}
