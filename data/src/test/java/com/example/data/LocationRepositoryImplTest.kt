package com.example.data

import com.example.data.location.LocationProvider
import com.example.data.location.LocationRepositoryImpl
import com.example.domain.location.LatLon
import com.example.domain.location.LocationUnavailableError
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class LocationRepositoryImplTest {

    private val locationProvider = mockk<LocationProvider>()
    private val repository = LocationRepositoryImpl(locationProvider)

    @Test
    fun `should return non null location`() {
        runBlocking {
            coEvery { locationProvider.getLastDeviceLocation() } returns LatLon(42.0, 42.0)

            val location = repository.getDeviceLocation()

            assertThat(location).isEqualTo(LatLon(42.0, 42.0))
        }
    }

    @Test
    fun `should throw LocationUnavailableError when location is null`() {
        runBlocking {
            coEvery { locationProvider.getLastDeviceLocation() } returns null

            assertThrows<LocationUnavailableError> { repository.getDeviceLocation() }
        }
    }
}
