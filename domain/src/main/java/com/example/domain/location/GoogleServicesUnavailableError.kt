package com.example.domain.location

data class GoogleServicesUnavailableError(val code: Int) : Throwable()
