package com.example.domain.location

interface LocationRepository {
    suspend fun getDeviceLocation(): LatLon
}
