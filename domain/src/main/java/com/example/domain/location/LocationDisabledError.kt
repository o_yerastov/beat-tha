package com.example.domain.location

class LocationDisabledError(val throwable: Throwable?) : Throwable()
