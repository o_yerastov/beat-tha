package com.example.domain.location

data class LatLon(
    val latitude: Double,
    val longitude: Double,
)
