package com.example.domain.venue

data class Category(
    val id: String,
    val name: String,
    val primary: Boolean,
    val icon: String,
)
