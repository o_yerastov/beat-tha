package com.example.domain.venue

import com.example.domain.location.LatLon

data class Venue(
    val id: String,
    val name: String,
    val categories: List<Category>,
    val address: String,
    val location: LatLon?,
    val distance: Int,
    val photo: String?,
) {
    val category: Category? get() = categories.find { it.primary } ?: categories.firstOrNull()
}
