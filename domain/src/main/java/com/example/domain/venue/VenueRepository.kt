package com.example.domain.venue

import com.example.domain.location.LatLon

interface VenueRepository {
    suspend fun fetchVenues(latLon: LatLon): List<Venue>
    suspend fun fetchVenuePhoto(id: String): String?
}
