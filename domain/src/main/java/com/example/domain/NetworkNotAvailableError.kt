package com.example.domain

import java.io.IOException

class NetworkNotAvailableError : IOException()
